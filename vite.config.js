import { fileURLToPath, URL } from "node:url"

import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"

// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin

import {gitDescribeSync} from 'git-describe'

const commitHash = gitDescribeSync().raw

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
		vue(),
	],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  define: {
    'APP_VERSION': JSON.stringify(commitHash)
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import "@/scss/main.scss";`
      }
    }
  },  
})
