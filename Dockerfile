# "builder": this is the container we'll uso to BUILD our VueJS frontend
#            As such, it requires a working NodeJS, so we run a node:alpine variant
# vvvvvvvv building starts here vvvvvvvvvvv
FROM node:16-alpine as builder

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .

# If you are building your code for production
#RUN npm ci --only=production
RUN apk add git
run npm install -g npm@latest
RUN npm install
RUN npm run build
# ^^^^^^ building finished ^^^^^^^

## This is the REAL container, where the web-server will run and serve our APP
## As the APP has been "builded" it's simply a bunch of HTML, JS and CSS so... we
## doesn't need anything more than a standard Alpine. We'll only add a web-server
## vvvvvvvvvvvvvvvvvvvvvvvvvvvv
FROM alpine:3.18.2

##### Install lighttpd and configure it
RUN apk add lighttpd
#
# Enable mod_rewrite to easy "/" access (when no explicit index.html is provided in the URL)
RUN sed -i '/#.*"mod_rewrite/s/^#//' /etc/lighttpd/lighttpd.conf
RUN echo 'url.rewrite-if-not-file = ( "/ndpid-rt-viewer/.*" => "/ndpid-rt-viewer/index.html" )' >> /etc/lighttpd/lighttpd.conf
#
# disable log write, and redirect them to /dev/stdout
RUN sed -i '/server.errorlog/s/^server/#server/' /etc/lighttpd/lighttpd.conf
RUN sed -i '/accesslog.filename/s/^accesslog/#accesslog/' /etc/lighttpd/lighttpd.conf
RUN echo 'server.errorlog="/dev/stdout"' >> /etc/lighttpd/lighttpd.conf
RUN echo 'accesslog.filename="/dev/stdout"' >> /etc/lighttpd/lighttpd.conf
#
# let's listen on port 3000
RUN echo 'server.port=3000' >> /etc/lighttpd/lighttpd.conf

RUN mkdir -p /var/www/localhost/htdocs/ndpid-rt-viewer

WORKDIR /var/www/localhost/htdocs/ndpid-rt-viewer

COPY --from=builder /usr/src/app/dist .

EXPOSE 3000

# Run lighttpd
CMD ["lighttpd", "-f", "/etc/lighttpd/lighttpd.conf", "-D"]

# ^^^^^^ fine secondo step ^^^^^
