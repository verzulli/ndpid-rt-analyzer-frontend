# ndpid-rt-analyzer-frontend

*Released under the A-GPL License (see enclosed [LICENSE.txt](LICENSE.txt))*

## 1 - What's this?

This is the WebUI of the [ndpid-rt-analyzer](https://github.com/verzulli/ndpid-rt-analyzer) project. If you need to interact with `ndpid-rt-analyzer` with something more than the provided CLI, than this project will help. Actually, this project can also interact directly with the `nDPIsrvd` component provided directly by [nDPId](https://github.com/utoni/nDPId).

## 2 - so... what's, exactly, technically speaking?

This is a VUE-3, [VueJS](https://vuejs.org/) project, built with [Vite](https://vitejs.dev/), and using [Vuetify v. 3](https://next.vuetifyjs.com/en/) framework.

You can easily run it on your system

## 3 - How to run

If you're interested in this project and want to get it a try, you have to get the sources and run the application from them. This tipically require the configuration of the whole development environment (don't worry! It can be done 100% automatically!)

In short:

1. clone the repo
2. install the dependencies via `npm install`
3. create the `.env.development.local` file, based on the provided `.sample`. Here you'll tipically point to the `ndpi-rt-analyzer` backend running somewhere, and providing relevant web-services
4. fire an `npm run dev`
5. point your browser to `http://localhost:5137`, aka: the development-server kindly provided by Vite

## 4 - Something else I should know?

If you want to ship the application somewhere, you need to do your home-work. The relevant set of items to be shipped (HTMLs, CSSs, JSs) can be build by Vite with:

* `npm run build`

and, afterwards, retrieved in the `[...]/dist` subfolder.

Remember that in production WebServices will be pointed to the URL/PATH reported in the `.env.production` **VITE_APP_WS_BACKEND** variable, so you need to carefully plan what your *development environment* and *production environmenté are, exactly. But, again, this is left to you, as an exercise :-)