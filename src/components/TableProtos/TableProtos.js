import axios from 'axios'
import { mapStores } from 'pinia'
import { useRTAnalyzerUIStore } from '@/store/store'
import EventBus from "@/components/EventBus"

export default {
  name: 'TableProtos',
  components: {
  },
  data: () => ({
    dialogProtoDetailShow: false,
    dialogFlowDetailShow: false,
    dialogDetailTitle: '',
    dialogFlowDetailContent: '',
    riskDetailsIDs: [],
    mainTable: {
      "items": [],
      "headers": [
        { label: 'Proto', key: 'proto' },
        { label: 'Num', key: 'num' }
      ]
    },
    subTblProtos: {
      "items": [],
      "headers": [
        { label: 'flow_id', key: 'flow_id' },
        { label: 'src_ip', key: 'src_ip' },
        { label: 'src_port', key: 'src_port' },
        { label: 'dst_ip', key: 'dst_ip' },
        { label: 'dst_port', key: 'dst_port' },
        { label: 'l4_proto', key: 'l4_proto' },
        { label: 'hostname', key: 'ndpi_hostname' }
      ]
    }
  }),
  computed: {
    // retrieving store data, with option-API
    // (ref: https://pinia.vuejs.org/cookbook/options-api.html )
    ...mapStores(useRTAnalyzerUIStore)    
  },
  mounted () {
    console.log("[TableProtos] Mounting APP")  

    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('COUNTERS', (msg) => { this.updateCounters(msg)})
  },
  methods: {
    updateCounters: async function (counters) {
      // console.log('[TableProtos/updateCounters] Updating counters ==>' + JSON.stringify(counters.parser.protos) + ']')
      const protoCounters = counters.parser.protos

      // clear previous table values
      this.mainTable.items = []

      // populate table, ordered by values
      const _p = Object.keys(protoCounters)
      _p.sort( (a, b) => { return protoCounters[b] - protoCounters[a] })

      _p.forEach( (e) => {
        const _tmp = { 
          'proto': e,
          'num': protoCounters[e]
        }
        this.mainTable.items.push(_tmp)
      })
    },

    async getDetails (i) {
      const selectedProto = i.item.proto

      console.log('[TableProtos/getDetails] Click triggere on proto: [' + i.item.proto + ']')

      // let's fetch remote riskmap
      this.sWSocketBackend = import.meta.env.VITE_APP_WSOCKET_BACKEND
  
      console.log("[TableProtos/getDetails] Retrieving current proto map")
      try {
        const res = await axios.get( 
          this.rtUIStore.APIBackend + '/fetchprotomap',
          {},
          {
            timeout: 10 * 1000
          }
        )
        // console.log("[TableProtos/vonDblClick] Got >>>" + JSON.stringify(res.data) + "<<<")
        if (res.data) {
          this.flowStore = res.data.flows

          // let's empty previous table
          this.subTblProtos.items = []

          if (Object.hasOwn(res.data.protos, selectedProto)) {
            const _protoFlowIDs = res.data.protos[selectedProto]
            _protoFlowIDs.forEach( (id) => {
              const _row = {
                'flow_id': id,
                'src_ip': this.flowStore[id].src_ip,
                'src_port': this.flowStore[id].src_port,
                'dst_ip': this.flowStore[id].dst_ip,
                'dst_port': this.flowStore[id].dst_port,
                'l4_proto': this.flowStore[id].l4_proto,
                'ndpi_hostname': this.flowStore[id].ndpi_hostname
              }
              this.subTblProtos.items.push(_row)
            })
          }
        }
      } catch (e) {
        console.log("[TableProtos/getDetails] Error: [" + e + "]")
      }

      console.log('[TableProtos/getDetails] Proto.name [' + selectedProto + ']')
      this.dialogProtoDetailShow = true
      this.dialogDetailTitle = selectedProto
    },

    showProtoDetails: function (i) {
      console.log("[TableProtos/showProtoDetails] Click triggered on flow_id:[" + i.item.flow_id + "]")

      EventBus.emit('SHOW_FLOW_VIEWER', {
        'jsonData': this.flowStore[i.item.flow_id],
        'title': 'Flow detail for [' + i.item.flow_id +']'
      })
    } 
  }
}
