import { use } from 'echarts/core'
import { CanvasRenderer } from 'echarts/renderers'
import { LineChart } from 'echarts/charts'
import {
  GridComponent,
  TitleComponent,
  TooltipComponent,
  LegendComponent,
} from 'echarts/components'
import VChart from 'vue-echarts'
import EventBus from "@/components/EventBus"

use([
  CanvasRenderer,
  LineChart,
  GridComponent,
  TitleComponent,
  TooltipComponent,
  LegendComponent,
])

export default {
  name: 'TimeLineChart',
  components: {
    'v-timeline-chart': VChart,
  },
  data: () => ({
    prevCounters: null,
    chartOptions: {
      legend: {
        orient: 'horizontal',
        left: 'center',
        top: 'bottom',
        data: ['new', 'detected', 'detection-update', 'guessed', 'update', 'not-detected', 'analyse', 'idle', 'end'],
      },
      tooltip: {
        trigger: 'axis',
        confine: 'true'
        // formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      xAxis: [
        {
          type: 'time',
          data: []
        }
      ],
      yAxis: [
        {
          type: 'value',
          position: 'left',
          // min: 0,
          // max: 'dataMax',
          splitLine: {
            show: false
          }
        }
      ],      
      series: [
        {
          name: 'new',
          type: 'line',  
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'detected',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'detection-update',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'guessed',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'update',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'not-detected',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'analyse',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'idle',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },
        {
          name: 'end',
          type: 'line',             
          symbol: 'none',
          xAxisIndex: 0,
          yAxisIndex: 0,
          stack: 'tutti',
          data: []
        },        
      ],
    }
  }),
  computed: {},
  mounted () {
    console.log("[TimeLineChart] Mounting APP")

    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('COUNTERS', (msg) => { this.addValues(msg)})
  },
  methods: {
    addValues: async function (valuesObj) {
      // console.log("[TimeLineChart/addValues] Got new counter values ==>" + JSON.stringify(valuesObj) + "<==")
      const curValues = valuesObj.parser.flows

      const curTS = Date.now()
      const ptrSeries = this.chartOptions.series
      if (this.prevCounters) {
        const d = {
          'new': curValues.new - this.prevCounters.new,
          'detected': curValues.detected - this.prevCounters.detected,
          'detection-update': curValues['detection-update'] - this.prevCounters['detection-update'],
          'guessed': curValues.guessed - this.prevCounters.guessed,
          'update': curValues.update - this.prevCounters.update,
          'not-detected': curValues['not-detected'] - this.prevCounters['not-detected'],
          'analyse': curValues['analyse'] - this.prevCounters['analyse'],
          'idle': curValues['idle'] - this.prevCounters['idle'],
          'end': curValues['end'] - this.prevCounters['end']
        }
        if ((d.new + d.detected + d['detection-update'] + d.guessed + d.update + d['not-detected'] + d.analyse + d.idle + d.end) !== 0) {
          ptrSeries[0].data.push([curTS, d.new])
          ptrSeries[1].data.push([curTS, d.detected])
          ptrSeries[2].data.push([curTS, d['detection-update']])
          ptrSeries[3].data.push([curTS, d.guessed])
          ptrSeries[4].data.push([curTS, d.update])
          ptrSeries[5].data.push([curTS, d['not-detected']])
          ptrSeries[6].data.push([curTS, d.analyse])
          ptrSeries[7].data.push([curTS, d.idle])
          ptrSeries[8].data.push([curTS, d.end])
        }
      }
      this.prevCounters = { ...curValues}              
    }
  }
}
