import { use } from 'echarts/core'
import { CanvasRenderer } from 'echarts/renderers'
import { PieChart } from 'echarts/charts'
import {
  TitleComponent,
  TooltipComponent,
  LegendComponent,
} from 'echarts/components'
import axios from 'axios'
import VChart from 'vue-echarts'
import { mapStores } from 'pinia'
import { useRTAnalyzerUIStore } from '@/store/store'
import EventBus from "@/components/EventBus"

use([
  CanvasRenderer,
  PieChart,
  TitleComponent,
  TooltipComponent,
  LegendComponent,
])

export default {
  name: 'PieChartRisks',
  components: {
    'v-pie-chart-risks': VChart,
  },
  data: () => ({
    dialogRiskDetailShow: false,
    dialogFlowDetailShow: false,
    dialogDetailTitle: '',
    dialogFlowDetailContent: '',
    tblRisks: {
      "items": [],
      "headers": [
        { label: 'flow_id', key: 'flow_id' },
        { label: 'src_ip', key: 'src_ip' },
        { label: 'src_port', key: 'src_port' },
        { label: 'dst_ip', key: 'dst_ip' },
        { label: 'dst_port', key: 'dst_port' },
        { label: 'ndpi_hostname', key: 'ndpi_hostname' }
      ]
    },
    pieOptions: {
      color: ['#ff8700', '#ffd300', '#deff0a', '#a1ff0a', '#0aff99', '#0aefff', '#147df5', '#580aff', '#be0aff'],
      title: {
        text: 'Risks events',
        left: 'center',
        show: false
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)',
      },
      series: [
        {
          name: 'Risks',
          type: 'pie',
          label: {
            show: true,
            formatter: '{b}:\n{c} ({d}%)',
          },
          data: [],
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    }
  }),
  computed: {
    // retrieving store data, with option-API
    // (ref: https://pinia.vuejs.org/cookbook/options-api.html )
    ...mapStores(useRTAnalyzerUIStore)       
  },
  mounted () {
    console.log("[PieChartRisks] Mounting APP")

    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('COUNTERS', (msg) => { this.updateCounters(msg)})    
  },
  methods: {
    updateCounters: async function (counters) {
      const risksCounters = counters.parser.risks
      // console.log("[PieChartRisks/updateCounters] Got new counter values ==>" + JSON.stringify(risksCounters) + "<==")
      let ptrSeries = this.pieOptions.series[0].data
      // let's empty existing data
      ptrSeries.length=0

      for (const r in risksCounters) {
        ptrSeries.push({'name': r, 'value': risksCounters[r]})
      }
    },

    async vonDblClick (evt) {
      console.log('[PieChartRisk/vonDblClick] è un [' + evt.componentType + ']')

      // ignoring not-series events
      if (evt.componentType !== 'series') {
        return
      }
      // let's fetch remote riskmap
      this.sWSocketBackend = import.meta.env.VITE_APP_WSOCKET_BACKEND
  
      console.log("[PieChartRisk/vonDblClick] Retrieving current risk map")
      try {
        const res = await axios.get( 
          this.rtUIStore.APIBackend + '/fetchriskmap',
          {},
          {
            timeout: 10 * 1000
          }
        )
        // console.log("[PieChartRisk/vonDblClick] Got >>>" + JSON.stringify(res.data) + "<<<")
        if (res.data) {
          const selectedRisk = evt.name
          this.flowStore = res.data.flows

          // let's empty previous table
          this.tblRisks.items = []

          if (Object.hasOwn(res.data.riskmap, selectedRisk)) {
            const _riskFlowIDs = res.data.riskmap[selectedRisk]
            _riskFlowIDs.forEach( (id) => {
              const _row = {
                'flow_id': id,
                'src_ip': this.flowStore[id].src_ip,
                'src_port': this.flowStore[id].src_port,
                'dst_ip': this.flowStore[id].dst_ip,
                'dst_port': this.flowStore[id].dst_port,
                'ndpi_hostname': this.flowStore[id].ndpi_hostname
              }
              this.tblRisks.items.push(_row)
            })
          }
        }
      } catch (e) {
        console.log("[PieChartRisk/vonDblClick] Errore: [" + e + "]")
      }

      console.log('[PieChartRisk/vonDblClick] series.name [' + evt.name + ']')
      console.log('[PieChartRisk/vonDblClick] series.data ==>' + JSON.stringify(evt.data) + '<==')
      // this.$root.$emit('show_note_editor', evt.value)
      this.dialogRiskDetailShow = true
      this.dialogDetailTitle = evt.name
    },

    showFlowDetails: function (i) {
      console.log("[PieChartRisk/showFlowDetails] Click triggered on flow_id:[" + i.item.flow_id + "]")

      EventBus.emit('SHOW_FLOW_VIEWER', {
        'jsonData': this.flowStore[i.item.flow_id],
        'title': 'Flow details for [' + i.item.flow_id + ']'
      })
    }
  } 
}
