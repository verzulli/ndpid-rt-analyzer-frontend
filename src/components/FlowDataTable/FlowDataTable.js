import dateFormat from "dateformat"
import { mapStores } from 'pinia'
import { useRTAnalyzerUIStore } from '@/store/store'
import { contains } from 'cidr-tools'
import EventBus from "@/components/EventBus"

function _getTimeStr(ms) {
  if (ms !== 0) {
    // ms is MICRO-seconds, so we need to /1000 to get MILLI-seconds
    const t = new Date(Math.ceil(ms/1000))
    return dateFormat(t, "MM:ss")
  } else {
    return '-'
  }
}

// Return "true" if passed ipToCkeck belong to ourNet networks
function _isLocal(ourNet, ipToCheck) {
  // let's default to "FALSE"
  try {
    if (contains(ourNet, ipToCheck)) {
      return true
    }
    return false
  } catch (e) {
    // somewhing bad happened. Return false
    return false
  }
}

export default {
  name: 'FlowDataTable',
  components: {
  },
  data: () => ({
    sWSBackend: '',
    onlyRiskyEvent: false,
    noEmptyProbes: false,
    dialogDetailShow: false,
    showOutgoingConnections: true,
    showIncomingConnections: true,
    dialogContent: '',
    counter: 0,
    eventStore: {},
    evts: {
      "items": 
        [],
      "headers": [
        { label: '#', key: 'counter', width: '3%', align: 'center' },
        { label: 'ID', key: 'flow_id', width: '5%', align: 'center' },
        { label: 'durations', key: 'durations', align: 'center' },
        { label: 'pkts', key: 'packets', width: '4%', align: 'center' },
        { label: 'src_ip', key: 'src_ip', align: 'center' },
        { label: 'dst_ip', key: 'dst_ip', align: 'center' },
        { label: 'ASN', key: 'asn', align: 'center' },
        { label: 'dst_port', key: 'dst_port', width: '5%', align: 'center' },
        { label: 'ndpi_proto', key: 'ndpi_proto', align: 'center' },
        { label: 'P', key: 'is_simple_probe', width: '2%', align: 'center' },
        { label: 'hostname', key: 'ndpi_hostname', width: '15%', align: 'center' },
        { label: 'risk', key: 'ndpi_totalRisk', width: '5%', align: 'center' }
      ],
    }
  }),
  computed: {
    // retrieving store data, with option-API
    // (ref: https://pinia.vuejs.org/cookbook/options-api.html )
    ...mapStores(useRTAnalyzerUIStore)    
  },
  mounted () {
    console.log("[FlowDataTable] Mounting APP")

    this.sWSBackend = import.meta.env.VITE_APP_WS_BACKEND
    console.log("[FlowDataTable] VITE_APP_WS_BACKEND=[" + this.sWSBackend + ']')

    // let's dump our current local network CIDRs, retrieved from store
    console.log("[FlowDataTable] Local net from store: [" + this.rtUIStore.ourNetworks.join('|') + "]")

    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('FLOW', (msg) => { this.addEvent(msg)})
  },
  methods: {
    addEvent: function (msg) {
      // console.log("[FlowDataTable/addValues] Got new event values ==>" + JSON.stringify(msg) + "<==")

      // let's store the received event in the main eventStore (to be able to show details, when "clicked")
      this.eventStore[msg.flow_id]=msg

      // now, let's populate the sub-object to be used to "fill" the table rows
      const myTableItem = {}
      try {
        // next if toggle "Only Risky events" is active, and we have a no-risk flow
        if (this.onlyRiskyEvent && msg.ndpi_totalRisk === 0) {
          return
        }

        // next, if toggle "No empty probes" is active, and we have a no-traffic flow (port-scan or similar...)
        if (this.noEmptyProbes && msg.is_empty_probe ) {
          return
        }

        // Let's populate the temporary object to later push to the main table data
        myTableItem.flow_id = msg.flow_id
        myTableItem.is_partial = msg.is_partial
        myTableItem.srcDuration = _getTimeStr(msg.srcDuration)
        myTableItem.dstDuration = _getTimeStr(msg.dstDuration)
        // src host data
        myTableItem.src_ip = msg.src_ip
        myTableItem.src_is_local = _isLocal(this.rtUIStore.ourNetworks, msg.src_ip)
        // if it's an outgoing connection and related switch is OFF, throw away
        if (myTableItem.src_is_local && ! this.showOutgoingConnections) {
          return
        }

        // populate GeoIP data
        myTableItem.src_ip_country = ''
        myTableItem.src_ip_asn = ''
        myTableItem.dst_ip_country = ''
        myTableItem.dst_ip_asn = ''

        if ('src_ip_country' in msg) {
          if ('countryCode' in msg.src_ip_country && typeof (msg.src_ip_country.countryCode) !== 'undefined') {
            myTableItem.src_ip_country = msg.src_ip_country.countryCode
          }          
        }
        if ('src_ip_asn' in msg) {
          if(typeof (msg.src_ip_asn.number) !== 'undefined') {
            myTableItem.src_ip_asn = msg.src_ip_asn.number + ' - ' + msg.src_ip_asn.name
          }            
        }
        if ('dst_ip_country' in msg) {
          if ('countryCode' in msg.dst_ip_country && typeof (msg.dst_ip_country.countryCode) !== 'undefined') {
            myTableItem.dst_ip_country = msg.dst_ip_country.countryCode
          }
        }
        if ('dst_ip_asn' in msg) {
          if(typeof (msg.dst_ip_asn.number) !== 'undefined') {
            myTableItem.dst_ip_asn = msg.dst_ip_asn.number + ' - ' + msg.dst_ip_asn.name
          }  
        }        

        myTableItem.src_port = msg.src_port
        myTableItem.src_num_packets = msg.src_num_packets

        myTableItem.dst_ip = msg.dst_ip
        myTableItem.dst_is_local = _isLocal(this.rtUIStore.ourNetworks, msg.dst_ip)
        // if it's an incoming connection and related switch is OFF, throw away
        if (myTableItem.dst_is_local && ! this.showIncomingConnections) {
          return
        }

        myTableItem.dst_port = msg.dst_port
        myTableItem.dst_num_packets = msg.dst_num_packets
        
        myTableItem.ndpi_proto = msg.ndpi_proto
        myTableItem.ndpi_totalRisk = msg.ndpi_totalRisk
        myTableItem.ndpi_hostname= msg.ndpi_hostname
        myTableItem.is_empty_probe = msg.is_empty_probe
        // myTableItem.src_traffic_density_avg = Math.round(msg.src_traffic_density_avg * 10000 ) / 10000
        // myTableItem.src_traffic_density_stdev = Math.round(msg.src_traffic_density_stdev * 10000 ) / 10000
        // myTableItem.dst_traffic_density_avg = Math.round(msg.dst_traffic_density_avg * 10000 ) / 10000
        // myTableItem.dst_traffic_density_stdev = Math.round(msg.dst_traffic_density_stdev * 10000 ) / 10000
        
        myTableItem.counter = ++this.counter

        this.evts.items.unshift(myTableItem)
        if (this.evts.items.length > 100) {
          this.evts.items.pop()
        }
      } catch (e) {
        console.error("[FlowDataTable/addEvent] Error: [" + e + "] ==>" + JSON.stringify(msg) + '<==')
      }      
    },

    getDetails: function (i) {
      console.log("[FlowDataTable/getDetails] Click triggered on flow_id:[" + i.item.flow_id + "]")
      EventBus.emit('SHOW_FLOW_VIEWER', {
        'jsonData': this.eventStore[i.item.flow_id],
        'title': 'Flow details for [' + i.item.flow_id + ']'
      })
    },

    _isRFC1918: function (ip) {
      return contains([
        '192.168.0.0/16',
        '172.16.0.0/12',
        '10.0.0.0/8'
      ], ip)
    }
  }
}
