import EventBus from "@/components/EventBus"
import JsonViewer from 'vue-json-viewer'

export default {
  name: 'FlowViewer',
  components: {
    'json-viewer': JsonViewer
  },
  data: () => ({
    sWSBackend: '',
    dialogFlowDetailShow: false,
    dialogFlowDetailContent: 'Some dummy content',
    jsonToDisplay: {},
    dialogTitle: 'ndpid-rt-analyzer: Flow detail'
  }),
  computed: {},
  mounted () {
    console.log("[FlowViewer] Mounting APP")

    // gestore dell'evento che arriva dal componente principale
    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('SHOW_FLOW_VIEWER', (flow) => { this.openViewer(flow) })
  },
  methods: {
    openViewer: function(flow) {
        console.log("[FlowViewer/openViewer] Triggering event handler. Opening modal...")
        this.dialogFlowDetailContent = JSON.stringify(flow.jsonData, null, 2),
        this.jsonToDisplay = flow.jsonData
        this.dialogTitle = flow.title 
        this.dialogFlowDetailShow = true
    }
  }
}
