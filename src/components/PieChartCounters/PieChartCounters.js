import { use } from 'echarts/core'
import { CanvasRenderer } from 'echarts/renderers'
import { PieChart } from 'echarts/charts'
import {
  TitleComponent,
  TooltipComponent,
  LegendComponent,
} from 'echarts/components'
import VChart from 'vue-echarts'
import EventBus from "@/components/EventBus"

use([
  CanvasRenderer,
  PieChart,
  TitleComponent,
  TooltipComponent,
  LegendComponent,
])

export default {
  name: 'PieChartCounters',
  components: {
    'v-pie-chart-counters': VChart,
  },
  data: () => ({
    sWSBackend: '',
    pieOptions: {
      title: {
        text: 'Flow events',
        left: 'center',
        show: false
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'vertical',
        left: 'left',
        top: 'top',
        show: false,
        data: ['new', 'detected', 'detection-update', 'guessed', 'update', 'not-detected', 'analyse', 'idle', 'end'],
      },
      series: [
        {
          name: 'Flow events',
          type: 'pie',
          // radius: ['20%','40%'],
          // center: ['50%', '30%'],
          label: {
            show: true,
            // formatter: '{b}:\n{c} ({d}%)',
            formatter: '{b}: {c}',
          },
          data: [],
          emphasis: {
            itemStyle: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },
        },
      ],
    }
  }),
  computed: {},
  mounted () {
    console.log("[PieChartCounters] Mounting APP")

    // attaching event-handler, to trap incoming messages from main home component
    EventBus.on('COUNTERS', (msg) => { this.updateCounters(msg)})    
  },
  methods: {
    updateCounters: async function (flowCounters) {
      // console.log("[PieChartCounters/updateCounters] Got new counter values ==>" + JSON.stringify(flowCounters) + "<==")
      let resData = flowCounters.parser.flows
      let ptrSeries = this.pieOptions.series[0].data
      // let's empty existing data
      ptrSeries.length=0

      // let's push new data
      ptrSeries.push({'name': 'new', 'value': resData.new})
      ptrSeries.push({'name': 'detected', 'value': resData.detected})
      ptrSeries.push({'name': 'detection-update', 'value': resData['detection-update']})
      ptrSeries.push({'name': 'guessed', 'value': resData.guessed})
      ptrSeries.push({'name': 'update', 'value': resData.update})
      ptrSeries.push({'name': 'not-detected', 'value': resData['not-detected']})
      ptrSeries.push({'name': 'idle', 'value': resData.idle})
      ptrSeries.push({'name': 'analyse', 'value': resData.analyse})
      ptrSeries.push({'name': 'end', 'value': resData.end})
    }
  } 
}
