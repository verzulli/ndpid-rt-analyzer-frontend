import PieChartCounters from '@/components/PieChartCounters/index.vue'
import PieChartRisks from '@/components/PieChartRisks/index.vue'
import TableProtos from '@/components/TableProtos/index.vue'
import TimeLineChart from '@/components/TimeLineChart/index.vue'
import FlowDataTable from '@/components/FlowDataTable/index.vue'
import FlowViewer from '@/components/FlowViewer/index.vue'

export default {
  name: 'EngineStats',
  components: {
    'pie-chart-counters': PieChartCounters,
    'timeline-chart': TimeLineChart,
    'flow-data-table': FlowDataTable,
    'pie-chart-risks': PieChartRisks,
    'table-protos': TableProtos,
    'rt-flow-viewer': FlowViewer
  },
  data() {
    return {
    }
  },
  computed: {},
  mounted: async function () {
    console.log("[EngineStats/mounted] Mounting APP")
  },

  // We're in VUE3, with `options` API. Remember to **AVOID** using
  // arrow functions in "methods", as for:
  // https://vuejs.org/guide/essentials/reactivity-fundamentals.html#declaring-reactive-state
  methods: {}
}
