import axios from 'axios'
import { mapStores } from 'pinia'
import { useRTAnalyzerUIStore } from '@/store/store'
import EventBus from "@/components/EventBus"

// import { mdiCogTransferOutline } from '@mdi/js'

let _timerCheckWebSocket = null

export default {
  name: 'Home',
  components: {
  },
  data() {
    return {
      app_version: 'DEVEL',
      sAPIBackend: '',
      sAPIBackendText: import.meta.env.VITE_APP_WS_BACKEND,
      APIbackendConfigured: false,

      sWebSocketBackend: '',
      sWebSocketBackendText: import.meta.env.VITE_APP_WSOCKET_BACKEND,
      webSocketBackendConfigured: false,
      wSocketConnection: null,
      webSocketState: 'unknown',

      remoteHost: '172.16.16.1',
      remotePort: '7000',

      ourNetworksString: '', // to be used as input/display in the panel/WebUI
      // lOurNetworks: [],  // the array of CIDRs to be considered
      receiverUDPConnected: false,
      receiverTCPConnected: false,
      openSettingsPanel: false
    }
  },
  computed: {
    // retrieving store data, with option-API
    // (ref: https://pinia.vuejs.org/cookbook/options-api.html )
    ...mapStores(useRTAnalyzerUIStore)
  },

  mounted: async function () {
    // eslint-disable-next-line no-undef
    this.app_version = APP_VERSION
    
    console.log("[Home/mounted] Mounting APP [" + this.app_version + "]")
  },

  beforeUnmount: async function () {
    console.log("[Home/beforeUnmount] Clearing environment...")
    if (this.wSocketConnection) {
      console.log("[Home/beforeUnmount] Terminating websocket handle")
      this.wSocketConnection.close()
    }
    if (_timerCheckWebSocket) {
      clearInterval(_timerCheckWebSocket)
      console.log("[Home/beforeUnmount] Destroing existing Interval")
    } 
  },
  
  // We're in VUE3, with `options` API. Remember to **AVOID** using
  // arrow functions in "methods", as for:
  // https://vuejs.org/guide/essentials/reactivity-fundamentals.html#declaring-reactive-state
  methods: {
    connectToAPIServer: async function() {
      console.log("[Home/connectToAPIServer] Retrieving receiver status")
      try {
        const res = await axios.get( 
            this.sAPIBackend + '/statusreceiver',
          {},
          {
            timeout: 10 * 1000
          }
        )
        console.log("[Home/connectToAPIServer] Got >>>" + JSON.stringify(res.data) + "<<<")
        if (res.data.udp) {
          this.receiverUDPConnected = true
        }
        if (res.data.tcp) {
          this.receiverTCPConnected = true
        }
        this.$waveui.notify('Status succesfully retrieved!', 'success')
      } catch (e) {
        console.log("[Home/connectToAPIServer] Errore: [" + e + "]")
        this.$waveui.notify('Unable to fetch initial API-SEVER data. Check backend status! [' + e + ']', 'error')
      }
  
      console.log("[Home/connectToAPIServer] Fetching initial counter values")
      try {
      const res = await axios.get(
          this.sAPIBackend + '/fetchcounters',
          {},
          {
            timeout: 10 * 1000 // 10... seconds
          }
        )
        this.processMsgCounters({'parser': res.data.parser})
        this.$waveui.notify('Counters succesfully retrieved!', 'success')
      } catch (e) {
        console.error("[sAPIBackend] Errore: [" + e + "]")
        this.$waveui.notify('Unable to fetch initial counters. Check API backend!', 'error')
      }
  
      _timerCheckWebSocket = setInterval(() => { if (this.wSocketConnection) {
        // readyState => https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
        this.webSocketState = this.wSocketConnection.readyState // 1 => "OPEN (connected)"
      } }, 500)
  
    },
    setAPIBackend: function() {
      // TODO: add some URL type-checking
      this.sAPIBackend = this.sAPIBackendText
      this.APIbackendConfigured = true
      this.$waveui.notify('API Server defined!', 'success')

      // store the API-server in the... store
      this.rtUIStore.APIBackend = this.sAPIBackend

      // connecting to API-server and retrieving initial counters/data
      this.connectToAPIServer()
    },
    unsetAPIBackend: function() {
      this.sAPIBackend = ''
      this.APIbackendConfigured = false

      // unset the API-server in the store
      this.rtUIStore.APIBackend = ''
    },

    setWebSocketBackend: function() {
      // TODO: add some URL type-checking
      this.sWebSocketBackend = this.sWebSocketBackendText

      // store the WS-server in the... store
      this.rtUIStore.webSocketBackend = this.sWebSocketBackend

      this.webSocketBackendConfigured = true
      this.$waveui.notify('WebSocket Server defined!', 'success')
    },
    unsetWebSocketBackend: function() {
      this.sWebSocketBackend = ''
      this.webSocketBackendConfigured = false

      // unset the WS-server in the store
      this.rtUIStore.webSocketBackend = ''
    },

    startReceiver: async function() {
      console.log("[Home/startReceiver] requesting start tcpreceiver to [" + this.remoteHost + ":" + this.remotePort + "]")
      try {
        await axios.post( 
            this.sAPIBackend + '/starttcpreceiver',
          {
            'host': this.remoteHost,
            'port': this.remotePort
          },
          {
            timeout: 10 * 1000
          }
        )
        this.receiverTCPConnected = true
        this.$waveui.notify('remote nDPIsrvd connection, succesfully established!', 'success')

        // everything OK so.. let's auto-close the drawer, in 0,5 sec
        setTimeout( () => { this.openSettingsPanel = false }, 500)        

      } catch (e) {
        console.log("[Home/startReceiver] Errore: [" + e + "]")
        this.$waveui.notify('The server has been unable to connect to remote nDPIsrvd!', 'error', 6000)
      }      
    },
    stopReceiver: async function() {
      console.log("[Home/stopReceiver] requesting STOP receiver to [" + this.remoteHost + ":" + this.remotePort + "]")
      try {
        await axios.get( 
            this.sAPIBackend + '/stopreceiver',
          {},
          {
            timeout: 10 * 1000
          }
        )
        this.receiverTCPConnected = false
        this.receiverUDPConnected = false
      } catch (e) {
        console.log("[Home/stopReceiver] Errore: [" + e + "]")
      }      
    },      
    connectWS: function () {
      console.log("[home/connectWS] Starting connection to WebSocket Server")
      this.wSocketConnection = new WebSocket(this.sWebSocketBackend)
  
      // We need an Arrow-function, as we need to keep `this` access..
      this.wSocketConnection.onmessage = (msg) => {
        // console.log("[Home/hdlOnMsg] Got msg ==>" + msg.data + "<==")
        try {
          // remember that the real incoming message, is in the "data" property of "msg"
          const data = JSON.parse(msg.data)
          // console.log("[Home/hdlOnMsg] Got msg ==>" + JSON.stringify(data) + "<==")
          if (data.class === 'events') {
            this.processMsgEvent(data.content)
          } else if (data.class === 'counters') {
            this.processMsgCounters(data.content)
          } else {
            console.log("[home/hdlOnMsg] Unknown message class: [" + data.class + "]")
          }
        } catch (e) {
          console.error("[Home/hdlOnMsg] Bad websocket incoming message class [" + JSON.stringify(msg.data) + "]. Skipping")
        }
      }
  
      this.wSocketConnection.onopen = (event) => {
        console.log("[Home/hdlOpen] Got onopen evt ==>" + JSON.stringify(event) + "<==")
        this.$waveui.notify('WebSocket succesfully connected!', 'success')

        // everything OK so.. let's auto-close the drawer
        setTimeout( () => { this.openSettingsPanel = false }, 500)
      }

      this.wSocketConnection.onerror = (event) => {
        console.log("[Home/hdlError] Got onerror evt ==>" + JSON.stringify(event) + "<==")
        this.$waveui.notify('WebSocket connection error!', 'error')
      }
    },

    disconnectWS: function () {
      console.log("[home/disconnectWS] Disconnecting from websocket")
      this.wSocketConnection.close()
      this.$waveui.notify('Disconnected from websocket!', 'warning')
    },

    updateLocalNetworks() {
      console.log("[Home/updateLocalNetworks] updating local network as for string [" + this.ourNetworksString + "]")
      // TODO: sanity check for input string ourNetworksStringvshould be added

      // retrieve previous list from Pinia Store
      const _prevNet = this.rtUIStore.ourNetworks
      const _tmpNet = this.ourNetworksString.replaceAll(" ", "").split(',')

      console.log("[Home/updateLocalNetworks] Updating network list in store: From: [" + _prevNet.join('|') + "] to [" + _tmpNet.join('|') + "]")

      this.rtUIStore.ourNetworks = _tmpNet
      this.$waveui.notify('Local SUBNETs successfully configured!', 'success')

      // everything OK so.. let's auto-close the drawer, in 0,5 sec
      setTimeout( () => { this.openSettingsPanel = false }, 500)
    },
    
    processMsgEvent: function (msg) {
      // console.log("[Home/processMsgEvent] Forwarding event to flow-data-table component")

      EventBus.emit('FLOW', msg)
    },
    
    processMsgCounters: function (msg) {
      // console.log("[Home/processMsgCounters] Forwarding event to pie-chart component")
      
      // let's call the child methods, using related "ref" reported in the including TAG, in home
      EventBus.emit('COUNTERS', msg)
    }
  }
}
