import { defineStore } from 'pinia'

export const useRTAnalyzerUIStore = defineStore('rtUI', {
  state: () => (
      { 
        ourNetworks: [],  // the array of CIDRs to be considered
                          // LOCAL
        
        APIBackend: '',  // API-Server / REST-endpoint URL
                          // eg.: http://localhost:3000

        webSocketBackend: '', // Web-Socket endpoint URL
                               // eg: ws://localhost:3300
      }
  ),
  getters: {},
  actions: {}
})
 