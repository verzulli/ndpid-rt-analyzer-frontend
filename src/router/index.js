import { createRouter, createWebHistory } from "vue-router"
import EngineStats from '@/views/EngineStats/index.vue'
import FlowStats from '@/views/FlowStats/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      redirect: '/engine'
    },
    {
      path: "/engine",
      name: "engine",
      component: EngineStats,
    },    
    {
      path: "/flowstat",
      name: "flowstat",
      component: FlowStats,
    }    
  ],
})

export default router
