import { createApp } from 'vue'
import { createPinia } from 'pinia'
import MainApp from '@/views/Home/index.vue'
import router from './router'
import WaveUI from 'wave-ui'
import 'wave-ui/dist/wave-ui.css'
import 'font-awesome/css/font-awesome.min.css'

const pinia = createPinia()
const app = createApp(MainApp)

new WaveUI(app, {
  // Some Wave UI options.
})

app.use(pinia)
app.use(router)
app.mount('#app')
